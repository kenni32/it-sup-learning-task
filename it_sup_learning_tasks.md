### Learning Task #1: IT Support documentation

Task 1: Installing Corporate Software Bundle on Debian PC
Task 2: Adding New User Without Administrative Permissions
Task 3: Changing Admin Password and Storing it in some encrypted external place
Task 4: Onboarding New Debian PCs
Task 5: Migration Guide (Debian PC)

- Submit the documentation as a PDF or in a shared document format.
- Include all screenshots or screencasts as embedded media or as separate files linked in the documentation.

**Note**: All requirements are fictional and intended for testing purposes only. Use your imagination to create a realistic environment for this task.

---

### Learning Task #2: Create a Script for Adding a New User

#### Objective:
Develop a bash script that automates the process of adding a new user to a Linux system.

#### Requirements:
1. **User Information Variables:**
   - `USER_NAME`: The username for the new user.
   - `USER_GROUP`: Primary group for the user, typically the same as the username.
   - `SL_USER_GROUP`: Supplementary groups the user should be part of (e.g., sudo, adm, ssh).
   - `USER_COMMENT`: Comment field for the user, typically the same as the username or can be left empty.
   - `USER_KEY`: Public SSH key for the user.
   - `PASSWORD_HASH`: Password hash for the user.

2. **Commands to be Used:**
   - `groupadd`: To create a new user group.
   - `useradd`: To create a new user with specified options.
   - `mkdir`: To create a directory for the user's SSH keys.
   - `chown`: To change the ownership of the user's home and SSH directories.
   - `chmod`: To set permissions for the user's home and SSH directories.
   - `echo`: To add the user's SSH public key to the authorized_keys file.
   - `ls`: To list the directory contents and verify permissions.

3. **Script Logic:**
   - Define and initialize user-related variables.
   - Create the primary group for the new user.
   - Create the new user with the specified primary and supplementary groups, home directory, comment, and password hash.
   - Set up the .ssh directory in the user's home directory.
   - Set the appropriate ownership and permissions for the .ssh directory and the authorized_keys file.
   - Add the user's public SSH key to the authorized_keys file.
   - Verify the directory and file permissions.

4. **Safety and Testing:**
   - Implement a `doIt` variable to optionally echo commands for testing without execution.

5. **Script Execution:**
   - Ensure the script is executable.
   - Provide instructions on how to run the script.

6. **Additional Considerations:**
   - Handle potential errors and edge cases (e.g., existing user or group).
   - Ensure the script is idempotent and can be run multiple times without causing issues.

---

### Learning Task #3: Install WordPress on Debian with Nginx and Document the Process for Future Support

#### Objective:
Install WordPress on a Debian server using Nginx as the web server with HTTPS. Create comprehensive documentation for the installation process and ongoing support.

---

### Learning Task #4: Deploy a Modern Monitoring Stack

#### Objective:
Deploy a modern monitoring stack on a Debian server using Prometheus, Grafana, Alertmanager, and Node Exporter. Ensure that Node Exporter and all web UIs are secured with HTTPS.

### Learning Task #5: Deploy a GLPI

#### Objective:
Deploy a GLPI, and add some ENDPOINTS.