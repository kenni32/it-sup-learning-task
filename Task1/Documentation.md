		
# Setup System
First, you need to install latest stable version of Debian. You can do this on official Debian site https://www.debian.org/distrib/ ![Image](./Images/DebianIso.png). It`s better to install complete installation image, cause you may not have internet connection. You will have 64-bit system, so install 64-bit version.
After install you need to setup your user to sudoers, cause you need to download corporate software. For this you need to enter root, by typing:

```
su -l
<PASSWORD>
adduser <USERNAME> sudo
```
#CHANGE TO VISUDO METHOD

Then, restart pc, for applying changes.
Now, your user can use sudo. It`s time to update system

```
sudo apt upgrade

```

# Install Corporate Software Bundle
You need GIT, version control system:

```
sudo apt install git

```

You will use Docker, for developing, shipping, and running applications in containers. 

```
sudo apt install docker

```
	
Now, install Visual Studio Code, open-source and lightweight, but powerful source code editor. For that, go to the https://code.visualstudio.com/Download, and install the .deb file ![Image](./Images/VS_CODE_DOCUMENTATION.png). You need to extract content, use this command:

```
sudo apt install ./<path-to-deb-file>

```

# Setuping system and users
After that, you need to add user, without administrative permissions. To do this, you need to type this lines in terminal:

```
sudo adduser <USERNAME>

```

Also, you can change root password, and if you do this, save the hash of password somewhere else:

```
sudo passwd
<PASSWORD>
<NEWPASSWORD>
sudo cat /etc/shadow | grep root > passwd
```


# Onboarding Debian PC

Always know, what command do, and why you need to use it with sudo
Backup your system every week, or two. And try to setup from backup, to see if backup is not corrupted
# Migration Guide
So you need to do, is backup your system. To do this, launch backup_script.sh, in folder Scripts
# Nginx 
For settuping nginx, you need to install it. 

```
sudo apt install nginx
```

	
Then you need to find nginx bin
#CHANGE BIN TO SYSTEMCTL LAUNCH
```
sudo nginx
```
Finaly we install and launch nginx
